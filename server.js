'use strict';

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const util = require('util');
const fs = require("fs");
const Guid = require("guid");

//Json Responds files
const employees = require("./data/employees");
const stats = require("./data/statistic");
const rootAPI = '/api/v1';
const PORT = process.env.PORT || 3000;
console.log(process.version)
app.use(bodyParser.json());

// on Empty Path
app.get(rootAPI, function (req, res) {
    console.log('Get All employees');
    res.send("Try /api/v1/employees")
});

// on Empty Path
app.get('/', function (req, res) {
    console.log('Get All employees');
    res.send("Try /api/v1/employees")
});

// Get All Employees
app.get(rootAPI + '/employees', function (req, res) {
    console.log('Get All employees');
    fs.readFile('./data/employees.json', function (err, data) {
        let jsonArray = JSON.parse(data);
        if (jsonArray.length > 0) {
            let response = [];
            jsonArray.map(function(obj){
                response.push({
                    "id": obj.id,
                    "firstName": obj.firstName,
                    "lastName" : obj.lastName,
                    "email": obj.email
                })
            })
            res
                .status(200)
                .json(response);
        }
    });
    //TODO: This is probably working, but what if the json array has millions of records:
    // Try to make it synchronuous

});

//TODO  Get Statistic : Write the GET function to read from static json file and
// respond a json array including all the objects


// Get one Employees on ID
app.get(rootAPI + '/employees/:id', function (req, res) {
    console.log('Get an employee based on id');
    // TODO get the id from request, find the object with id, return the object
    // With all of the properties

});

// TODO Write the POST function to get an object from Request Body, add it to
// employees json array (file) The object should have all the elements

// TODO Write the Delete function to get an id from URL, delete the object
// related to that id from json file

// TODO Write the PUT function to get an object (can be one or couple of
// elements) from Request Body , id from request URL, And modify the related
// object in the json file Running API Server
var serverHost = app.listen(PORT, function () {
    const host = serverHost
        .address()
        .address;
    const port = serverHost
        .address()
        .port;
    console.log('Backend running on ' + host + ':' + port);
});